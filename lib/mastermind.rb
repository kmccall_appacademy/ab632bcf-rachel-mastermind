class Code
  PEGS = {
    red: "R",
    green: "G",
    blue: "B",
    yellow: "Y",
    orange: "O",
    purple: "P"
  }

  attr_accessor :code
  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end


  def self.parse(str)
    pegs_arr = PEGS.values
    str_split = str.upcase.split("")
    pegs = str_split.map do |el|
      raise "Not a valid color" unless pegs_arr.include?(el)
      el
    end
    Code.new(pegs)
  end

  def self.random
    secret_code = ''
    colors = PEGS.values
    4.times do
      val = rand(0..5)
      secret_code << colors[val]
    end
    self.new(secret_code)
  end

  def [](val)
    @pegs[val]
  end


  def ==(input)
    return false unless input.is_a?(Code)
    if self.pegs == input.pegs
      return true
    else
      return false
    end
  end

  def exact_matches(input)
    count = 0
    input.pegs.each_with_index do |el, i|
      if self[i] == input[i]
        count += 1
      end
    end
    count
  end


   def near_matches(input)
     count = 0
     arr = []
     input.pegs.each do |el|
       if self.pegs.include?(el)
         count += 1 unless arr.include?(el)
         arr << el
       end
     end
     count - exact_matches(input)
   end


end

class Game
  attr_accessor :get_guess, :code
  attr_reader :secret_code


  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    "Enter your guess:"
    input = gets.chomp
    Code.new(input)
  end

  def display_matches(code)
    p code
    print "near marches = #{@secret_code.near_matches(code)}"
    print "exact matches = #{@secret_code.exact_matches(code)}"
  end

end
